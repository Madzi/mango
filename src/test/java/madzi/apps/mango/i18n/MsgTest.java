package madzi.apps.mango.i18n;

import java.util.ResourceBundle;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MsgTest {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("lang/mango");

    @Test
    public void testEnumCoveredByBundle() {
        bundle.keySet().forEach(key -> {
            Assertions.assertNotNull(Msg.findByKey(key));
        });
    }


    @Test
    public void testBundleCoveredByEnum() {
        Stream.<Msg>of(Msg.values()).forEach(msg -> {
            Assertions.assertTrue(bundle.containsKey(msg.key()));
        });
    }
}
