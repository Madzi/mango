package madzi.apps.mango.infra;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public final class BeanManager {

    private final Map<Class<?>, Object> beans;

    private BeanManager() {
        this.beans = new HashMap<>();
    }

    public <T> void registerBean(final Class<T> clazz, final T object) {
        this.beans.put(clazz, object);
    }

    public <T> T getBean(final Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }

    public static BeanManager initialize() {
        return new BeanManager();
    }
}
