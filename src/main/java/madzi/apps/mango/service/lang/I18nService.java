package madzi.apps.mango.service.lang;

import madzi.apps.mango.i18n.Msg;

public interface I18nService {

    String message(Msg msg);
}
