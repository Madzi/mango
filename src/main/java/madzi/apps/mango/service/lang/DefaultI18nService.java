package madzi.apps.mango.service.lang;

import java.util.ResourceBundle;
import madzi.apps.mango.i18n.Msg;

public class DefaultI18nService implements I18nService {

    private final ResourceBundle bundle;

    public DefaultI18nService(final ResourceBundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public String message(final Msg msg) {
        return bundle.getString(msg.key());
    }
}
