package madzi.apps.mango;

import java.util.ResourceBundle;
import madzi.apps.mango.i18n.Msg;
import madzi.apps.mango.infra.BeanManager;
import madzi.apps.mango.service.lang.DefaultI18nService;
import madzi.apps.mango.service.lang.I18nService;
import madzi.apps.mango.view.ViewManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration {

    private static final Logger logger = LoggerFactory.getLogger(Configuration.class);

    private static final BeanManager beanManager = BeanManager.initialize();

    public BeanManager getBeanManager() {
        return beanManager;
    }

    public void init() {
        var bundle = ResourceBundle.getBundle("lang/mango");
        beanManager.registerBean(ResourceBundle.class, bundle);

        var i18nService = new DefaultI18nService(bundle);
        beanManager.registerBean(I18nService.class, i18nService);
        logger.info(i18nService.message(Msg.LOG_INIT_APP));

        var viewManager = new ViewManager(beanManager);
        beanManager.registerBean(ViewManager.class, viewManager);
    }

    public void stop() {
        var i18nService = beanManager.getBean(I18nService.class);
        logger.info(i18nService.message(Msg.LOG_STOP_APP));
    }
}
