package madzi.apps.mango.i18n;

import java.util.stream.Stream;

public enum Msg {
    APP_TITLE("app.title"),

    LOG_INIT_APP("log.init.app"),
    LOG_STOP_APP("log.stop.app");

    private Msg(final String key) {
        this.key = key;
    }

    private final String key;

    public String key() {
        return key;
    }

    public static Msg findByKey(final String key) {
        return Stream.<Msg>of(values())
                .filter(msg -> msg.key.equals(key))
                .findAny()
                .orElse(null);
    }
}
