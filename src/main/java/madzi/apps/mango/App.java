package madzi.apps.mango;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import madzi.apps.mango.i18n.Msg;
import madzi.apps.mango.service.lang.I18nService;
import madzi.apps.mango.view.ViewManager;

public class App extends Application {

    private final Configuration configuration;

    public App() {
        this.configuration = new Configuration();
    }

    @Override
    public void init() {
        configuration.init();
    }

    @Override
    public void stop() {
        configuration.stop();
    }

    @Override
    public void start(final Stage stage) throws Exception {
        var beanManager = configuration.getBeanManager();
        var i18nService = beanManager.getBean(I18nService.class);

        var view = beanManager.getBean(ViewManager.class).load("main");
        stage.setScene(new Scene((Parent) view, 800.0, 600.0));
        stage.setTitle(i18nService.message(Msg.APP_TITLE));
        stage.show();
    }

    public static void main(final String... args) {
        launch(App.class, args);
    }
}
