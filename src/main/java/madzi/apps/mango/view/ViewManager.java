package madzi.apps.mango.view;

import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import madzi.apps.mango.infra.BeanManager;

public class ViewManager {

    private final BeanManager beanManager;
    private final FXMLLoader fxmlLoader;

    public ViewManager(final BeanManager beanManager) {
        this.beanManager = beanManager;
        this.fxmlLoader = new FXMLLoader();
        fxmlLoader.setResources(beanManager.getBean(ResourceBundle.class));
        fxmlLoader.setControllerFactory(beanManager::getBean);
    }

    public Node load(final String name) {
        try (final InputStream inputStream = getClass().getResourceAsStream("/view/" + name + ".fxml")) {
            return fxmlLoader.load(inputStream);
        } catch (final IOException ioException) {
            throw new RuntimeException("Unable to load FXML view: " + name, ioException);
        }
    }
}
