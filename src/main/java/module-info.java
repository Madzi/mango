module madzi.apps.mango {
    requires org.slf4j;
    requires org.slf4j.simple;
    requires javafx.controlsEmpty;
    requires javafx.controls;
    requires javafx.graphicsEmpty;
    requires javafx.graphics;
    requires javafx.baseEmpty;
    requires javafx.base;
    requires javafx.fxmlEmpty;
    requires javafx.fxml;

    exports madzi.apps.mango;
    opens madzi.apps.mango.view to javafx.fxml;
}
